package com.example.javaapp.Products;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository){
        this.productRepository = productRepository;
    }
    public List<Product> getProducts(){
        return productRepository.findAll();
    }

    public ResponseEntity<Object> createProducts(Product product){
        productRepository.save(product);
        HashMap<String, Object> map = new HashMap<>();
        map.put("status", true);
        return new ResponseEntity<Object>(map, HttpStatus.CREATED);
    }
}
